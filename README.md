# Curso de Computer Vision con R

## Instalaciones necesarias

## 1.-Lenguage de programación R versión Windows
Link de descarga <br/>
https://cran.r-project.org/bin/windows/base/<br/>
<br/>

## 2.-R Studio Para Windows 
Link de descarga <br/>
https://www.rstudio.com/products/rstudio/download/#download <br/>
<br/>

## 3.- H2O 
Link de descarga <br/>
http://h2o-release.s3.amazonaws.com/h2o/rel-wolpert/3/index.html <br/>
<br/>
Entrar a la carpeta donde se descargo el h2o y ejecutar el archivo java con el siguiente comando -> java -jar h2o.jar <br/>
Cuando se ejecute el archivo abrir en el navegador con la siguiente dirección -> http://192.168.1.77:54321 <br/

## 3.-TensorFlow 
Link de descarga <br/>
https://www.tensorflow.org/ <br/>

## Las instlaciones se hicieron descargardo anaconda3-5.1.0-Windows-x86_64 y Python 3.6.4
## Se utilizo comando para instalar tensorflow pip install --upgrade tensorflow
## Instalar Java SDK

## Instalación de paquetes curso: computer vision en RStudio

install.packages("caret") <br/>
install.packages("RColorBrewer")<br/>
install.packages("e1071")<br/>
install.packages("irlba")<br/>
install.packages("kernelknn")<br/>
install.packages("OpenImageR")<br/>
install.packages("randomForest")<br/>
install.packages("neuralnet")<br/>
install.packages("h2o")<br/>
install.packages("tensorflow")<br/>



